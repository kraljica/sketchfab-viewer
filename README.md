# Sketchfab Viewer
Simple app for generating iframes with Sketchfab Viewer API. Download full resolution screenshots on button click.

## Usage of Sketchfab Viewer API
getScreenShot();

### Author
Tatjana Milanovic

```
cd existing_repo
git remote add origin https://gitlab.com/kraljica/sketchfab-viewer.git
git branch -M main
git push -uf origin main
```